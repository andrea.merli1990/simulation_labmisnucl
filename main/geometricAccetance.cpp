#include "RunManager.h"
#include "GeneratorManager.h"
#include "TransportManager.h"
#include "GeometryManager.h"
#include "ISolid.h"
#include "ITransporter.h"
#include "IGenerator.h"
#include "Tube.h"
#include "StraightTransporter.h"
#include "DiskSourceGenerator.h"
#include <string>
#include <iostream>

int main(int argc, char** argv){
  int nEvents = 100;
  if(argc>1) nEvents = std::stoi(std::string(argv[1]));
  // Geometry
  GeometryManager::getInstance().setSizeWorldX(6.);
  GeometryManager::getInstance().setSizeWorldY(6.);
  GeometryManager::getInstance().setSizeWorldZ(6.);
  Tube geigerTube;
  geigerTube.setLength(4.6); //Length along z
  geigerTube.setRadius(0.96/2.); // radius in x-y
  const double distanceGeigerSource = 5.;
  const double positionSourceX = -2.5;
  geigerTube.setPosition(positionSourceX+distanceGeigerSource,0.,0.); // center of tube

  // Generator
  DiskSourceGenerator generator;
  GeneratorManager::getInstance().setGenerator(&generator);
  generator.setThickness(0.1);
  generator.setRadius(0.1);
  generator.setPosition(positionSourceX,0.,0.);

  // Transporter
  StraightTransporter transporter;
  TransportManager::getInstance().setTransporter(&transporter);

  // User function
  int lastEventID = -1;
  int nInGeiger = 0;
  TransportManager::getInstance().setUserFuncPreStep(
						     [&geigerTube,&lastEventID,&nInGeiger](){
						       Particle* p = TransportManager::getInstance().getCurrentParticle();
						       Event* ev = RunManager::getInstance().getCurrentEvent();
						       double x = p->X();
						       double y = p->Y();
						       double z = p->Z();
						       if( lastEventID!=ev->getID() && geigerTube.inSolid(x,y,z) ){
							 nInGeiger++;
							 lastEventID = ev->getID();
						       }
						     }
						     );

  // Run the simulation
  RunManager::getInstance().setNumberOfEvents(nEvents);
  RunManager::getInstance().run();

  std::cout << "Events in geiger: " << nInGeiger << std::endl;
  std::cout << "Total events: " << nEvents << std::endl;
  std::cout << "Geometric acceptance: " << nInGeiger/double(nEvents) << std::endl;

  return 0;
}
