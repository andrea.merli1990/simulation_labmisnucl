#include "Tube.h"
#include <cmath>

bool Tube::inSolid(const double& x,const double& y,const double& z){
  return std::fabs(z-fZ)<fLength/2. && std::sqrt(std::pow(x-fX,2)+std::pow(y-fY,2))<fRadius ;
}
