#include "GeneratorManager.h"
#include "RunManager.h"
#include <iostream>

void GeneratorManager::generate(){
  if(fGenerator!=nullptr){
    std::vector<Particle> particles = fGenerator->generate();
    RunManager::getInstance().getCurrentEvent()->addParticles(std::move(particles));
  }
  else std::cout << "ERROR in Generator Manager: you have to set the generator" << std::endl;
}
