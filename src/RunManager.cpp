#include "RunManager.h"
#include "Event.h"

void RunManager::run(){
  for(int i=0;i<fN;i++){
    Event event;
    event.setID(i);
    fCurrentEvent = &event;
    event.run();
  }
}
