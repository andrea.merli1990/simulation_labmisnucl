#include "DiskSourceGenerator.h"
#include <random>

std::vector<Particle> DiskSourceGenerator::generate(){
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> uniform(0,1);
  double radiusOrig = sqrt(uniform(gen))*fRadius;
  double angleOrig = uniform(gen)*2*M_PI;
  double xOrig = (uniform(gen)-0.5)*fThickness;
  double cosThetaMom = 2*uniform(gen)-1;
  double sinThetaMom = sqrt(1-pow(cosThetaMom,2));
  double phiMom = uniform(gen)*2*M_PI;
  Particle p;
  p.setPosition(xOrig+fPositionX,radiusOrig*std::sin(angleOrig)+fPositionY,radiusOrig*std::cos(angleOrig)+fPositionZ);
  p.setMomentum(sinThetaMom*std::cos(phiMom),sinThetaMom*std::sin(phiMom),cosThetaMom);
  return {p};
}
