#include "GeometryManager.h"
#include <cmath>

bool GeometryManager::inWorld(const double& x,const double& y,const double& z){
  return std::fabs(x)<fSizeWorldX/2. && std::fabs(y)<fSizeWorldY/2. && std::fabs(z)<fSizeWorldZ/2.;
}
