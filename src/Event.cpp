#include "Event.h"
#include "TransportManager.h"
#include "GeneratorManager.h"
#include <utility>

void Event::run(){
  GeneratorManager::getInstance().generate();
  while(!fParticles.empty()){
    TransportManager::getInstance().transport(fParticles[0]);
    fParticles.erase(fParticles.begin());
  }
}

void Event::addParticles(const std::vector<Particle>& particles){
  fParticles.insert(std::end(fParticles),std::begin(particles),std::end(particles));
}

void Event::addParticles(std::vector<Particle>&& particles){
  std::move(std::begin(particles), std::end(particles), std::back_inserter(fParticles));
  particles.clear();
}
