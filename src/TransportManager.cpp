#include "TransportManager.h"
#include <iostream>

void TransportManager::transport(Particle& p){
  fCurrentParticle = &p;
  if(fTransporter!=nullptr) 
    while(p.inWorld() && (!p.isAtRest()) ){
      if(fUserFuncPreStep) fUserFuncPreStep();
      fTransporter->transport(p);
      if(fUserFuncPostStep) fUserFuncPostStep();
    }
  else std::cout << "ERROR in TransporterManager: you have to set the transporter" << std::endl;
}
