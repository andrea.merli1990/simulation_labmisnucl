#include "StraightTransporter.h"
#include <iostream>
void StraightTransporter::transport(Particle& p){
  p.setPosition(p.X()+p.pX()/p.p()*fStep,
		p.Y()+p.pY()/p.p()*fStep,
		p.Z()+p.pZ()/p.p()*fStep);
}
