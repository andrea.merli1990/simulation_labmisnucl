#include "Particle.h"
#include "GeometryManager.h"
#include <cmath>

double Particle::p(){
  return std::sqrt(std::pow(fPx,2)+std::pow(fPy,2)+std::pow(fPz,2));
}

bool Particle::isAtRest(){
  return p()==0;
}

bool Particle::inWorld(){
  return GeometryManager::getInstance().inWorld(fX,fY,fZ);
}
