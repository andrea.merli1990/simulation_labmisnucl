#include "Manager.h"
#include "ISolid.h"
#include <vector>

#ifndef GEOMETRYMANAGER
#define GEOMETRYMANAGER

class GeometryManager : public Manager<GeometryManager> {
 public:
  void setSizeWorldX(const double& x){fSizeWorldX = x;};
  void setSizeWorldY(const double& y){fSizeWorldY = y;};
  void setSizeWorldZ(const double& z){fSizeWorldZ = z;};
  bool inWorld(const double& x,const double& y,const double& z);
 private:
  std::vector<ISolid*> fSolids={};
  double fSizeWorldX = 1;
  double fSizeWorldY = 1;
  double fSizeWorldZ = 1;
};

#endif
