#include "Manager.h"
#include "Particle.h"
#include "ITransporter.h"
#include <functional>

#ifndef TRANSPORTMANAGER
#define TRANSPORTMANAGER

class TransportManager : public Manager<TransportManager>{
 public:
  void transport(Particle& p);
  void setTransporter(ITransporter* transporter){fTransporter = transporter;};
  void setUserFuncPreStep(const std::function<void()>& func){fUserFuncPreStep = func;}
  void setUserFuncPostStep(const std::function<void()>& func){fUserFuncPostStep = func;}
  Particle* getCurrentParticle(){return fCurrentParticle;}
 private:
  ITransporter* fTransporter=nullptr;
  std::function<void()> fUserFuncPreStep;
  std::function<void()> fUserFuncPostStep;
  Particle* fCurrentParticle=nullptr;
};

#endif
