#include "Particle.h"
#include <vector>

#ifndef IGENERATOR
#define IGENERATOR

class IGenerator{
 public:
  virtual std::vector<Particle> generate()=0;
 private:
};

#endif
