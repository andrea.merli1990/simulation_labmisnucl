#include "Particle.h"

#ifndef ITRANSPORTER
#define ITRANSPORTER

class ITransporter{
 public:
  virtual void transport(Particle& p)=0;
  void setStep(double step){fStep=step;};
 protected:
  double fStep=1E-3;
};

#endif
