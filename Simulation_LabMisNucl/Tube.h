#include "ISolid.h"

#ifndef TUBE
#define TUBE

class Tube : public ISolid{
 public:
  void setPosition(const double& x,const double& y,const double& z){fX=x;fY=y;fZ=z;}
  void setLength(const double& length){fLength=length;}
  void setRadius(const double& radius){fRadius=radius;}
  bool inSolid(const double& x,const double& y,const double& z) override;
 private:
  double fLength = 1;
  double fRadius = 1;
  double fX=0;
  double fY=0;
  double fZ=0;
};

#endif
