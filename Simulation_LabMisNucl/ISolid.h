#ifndef ISOLID
#define ISOLID

class ISolid{
 public:
  virtual bool inSolid(const double& x,const double& y,const double& z)=0;
  virtual ~ISolid(){};
};

#endif
