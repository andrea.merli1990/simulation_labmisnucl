#ifndef MANAGER
#define MANAGER

template<class T>
class Manager{
 public:
  static T& getInstance(){
    static T fManager;
    return fManager;
  }
  
  Manager(Manager const&) = delete;             // Copy construct
  Manager(Manager&&) = delete;                  // Move construct
  Manager& operator=(Manager const&) = delete;  // Copy assign
  Manager& operator=(Manager &&) = delete;      // Move assign
  
 protected:
  Manager(){}
};

#endif
