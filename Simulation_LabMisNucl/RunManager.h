#include "Manager.h"
#include "Event.h"

#ifndef RUNMANAGER
#define RUNMANAGER

class RunManager : public Manager<RunManager> {
 public:
  void run();
  void setNumberOfEvents(int n){fN = n;};
  Event* getCurrentEvent(){return fCurrentEvent;};
 private:
  int fN=100;
  Event* fCurrentEvent;
};

#endif
