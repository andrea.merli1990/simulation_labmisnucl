#include "Manager.h"
#include "IGenerator.h"
#include <memory>

#ifndef GENERATORMANAGER
#define GENERATORMANAGER

class GeneratorManager : public Manager<GeneratorManager>{
 public:
  void generate();
  void setGenerator(IGenerator* generator){fGenerator = generator;}
 private:
  IGenerator* fGenerator = nullptr;
};

#endif
