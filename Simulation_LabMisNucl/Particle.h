#ifndef PARTICLE
#define PARTICLE

class Particle{
 public:
  void setMomentum(const double& px,const double& py,const double& pz){fPx=px;fPy=py;fPz=pz;};
  void setPosition(const double& x,const double& y,const double& z){fX=x;fY=y;fZ=z;};
  bool isAtRest();
  bool inWorld();
  double p();
  double X(){return fX;}
  double Y(){return fY;}
  double Z(){return fZ;}
  double pX(){return fPx;}
  double pY(){return fPy;}
  double pZ(){return fPz;}
 private:
  double fPx;
  double fPy;
  double fPz;
  double fX;
  double fY;
  double fZ;
};

#endif
