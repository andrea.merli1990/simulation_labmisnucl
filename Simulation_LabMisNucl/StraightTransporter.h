#include "ITransporter.h"
#include "Particle.h"

#ifndef STRAIGHTTRANSPORTER
#define STRAIGHTTRANSPORTER

class StraightTransporter final : public ITransporter {
 public:
  void transport(Particle& p) override;
};

#endif
