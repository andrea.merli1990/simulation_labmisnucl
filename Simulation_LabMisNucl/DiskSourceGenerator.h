#include "IGenerator.h"

#ifndef DISKSOURCEGENERATOR
#define DISKSOURCEGENERATOR

class DiskSourceGenerator : public IGenerator{
 public:
  std::vector<Particle> generate() override;
  void setThickness(const double& thickness){fThickness = thickness;};
  void setRadius(const double& radius){fRadius = radius;};
  void setPosition(const double& x, const double& y, const double& z){fPositionX=x;fPositionY=y;fPositionZ=z;}
 private:
  double fRadius=1;
  double fThickness=1;
  double fPositionX=0;
  double fPositionY=0;
  double fPositionZ=0;
};

#endif
