#include "Particle.h"
#include <vector>
#include <type_traits>

#if __cplusplus>=201703L
using std::enable_if_t;
using std::remove_cv_t;
using std::remove_reference_t;
#else
template< bool B, class T = void >
  using enable_if_t = typename std::enable_if<B,T>::type;
template< class T >
  using remove_cv_t = typename std::remove_cv<T>::type;
template< class T >
  using remove_reference_t = typename std::remove_reference<T>::type;
#endif

#ifndef EVENT
#define EVENT

class Event{
 public:
  //Event();
  void run();
  template<typename T, enable_if_t< std::is_same< remove_cv_t< remove_reference_t<T> > ,Particle>::value  > >
  void addParticle(T&& p){
    fParticles.push_back(std::forward<T>(p));
  };
  //void addParticle(const Particle& p);
  //void addParticle(Particle&& p);
  void addParticles(const std::vector<Particle>& particles);
  void addParticles(std::vector<Particle>&& particles);
  void setID(const int& id){fID=id;};
  int getID(){return fID;};
 private:
  std::vector<Particle> fParticles={};
  int fID;
};

#endif
